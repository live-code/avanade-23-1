import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { BasicExampleComponent } from './features/basic-example.component';
import { FormExampleComponent } from './features/form-example.component';
import { UsersComponent } from './features/users/users.component';
import { Page404Component } from './features/page404.component';
import { HomeComponent } from './features/home.component';
import { UsersDetailsComponent } from './features/users-details/users-details.component';
import { NavbarComponent } from './core/components/navbar.component';
import { SettingsComponent } from './features/settings.component';
import { UikitComponent } from './features/uikit/uikit.component';
import { HelloComponent } from './shared/components/hello.component';
import { CardComponent } from './shared/components/card.component';
import { UsersListComponent } from './features/users/components/users-list.component';
import { UsersFormComponent } from './features/users/components/users-form.component';
import { UsersListItemComponent } from './features/users/components/users-list-item.component';
import { MapQuestComponent } from './shared/components/map-quest.component';
import { SidePanelComponent } from './shared/components/side-panel.component';

@NgModule({
  declarations: [
    AppComponent,
    BasicExampleComponent,
    FormExampleComponent,
    UsersComponent,
    Page404Component,
    HomeComponent,
    UsersDetailsComponent,
    NavbarComponent,
    SettingsComponent,
    UikitComponent,
    HelloComponent,
    CardComponent,
    UsersListComponent,
    UsersFormComponent,
    UsersListItemComponent,
    MapQuestComponent,
    SidePanelComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'basic-example', component: BasicExampleComponent},
      { path: 'form-example', component: FormExampleComponent},
      { path: 'uikit', component: UikitComponent},
      { path: 'users', component: UsersComponent},
      { path: 'users/:userId', component: UsersDetailsComponent},
      { path: '404', component: Page404Component},
      { path: 'home', component: HomeComponent},
      { path: 'settings', component: SettingsComponent},
      { path: '', redirectTo: 'home', pathMatch: 'full'},
      { path: '**', redirectTo: '404'},
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
