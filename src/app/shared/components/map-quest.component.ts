import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-map-quest',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <img
      *ngIf="coords"
      [src]="'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center='+ coords.lat + ',' + coords.lng +'&size=200,100&zoom=' + zoom" alt="" width="100%">

  `,
})
export class MapQuestComponent {
  @Input() coords: { lat: string, lng: string } | undefined;
  @Input() zoom: number = 7
}

