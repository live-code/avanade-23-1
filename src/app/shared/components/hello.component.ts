import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-hello',
  template: `
    <p 
      [style.color]="color"
      [style.font-size.px]="size"
    >
      hello {{name}} {{size}}
      
    </p>
    
  `,
})
export class HelloComponent {
  @Input() size: number | string = 10;
  @Input() name: string | undefined;
  @Input() color = 'black'
}
