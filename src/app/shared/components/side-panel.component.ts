import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';
import { SidepanelService } from '../../features/users/services/sidepanel.service';

@Component({
  selector: 'app-side-panel',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="sidepanel"
         [ngClass]="{open: sidePanelService.isPanelOpened}">
      <i class="fa fa-times fa-2x"
         (click)="sidePanelService.closePanel(); close.emit()" 
         style="position: absolute; top: 10px; right: 10px"
      ></i>
      
      <ng-content></ng-content>
    </div>
    
  `,
  styles: [`
   .sidepanel {
      background-color: #dedede;      
      padding: 50px 10px;
      position: fixed;
      width: 300px;
      right: -300px;
      top: 0;
      bottom: 0;
      z-index: 10;
      transition: all 1s ease-in-out;
    }
    .sidepanel.open {
      right: 0;
    }
  `],
})
export class SidePanelComponent {
  @Output() close = new EventEmitter();

  constructor(
    public sidePanelService: SidepanelService,
  ) {
  }
}
