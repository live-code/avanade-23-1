import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  template: `
    <div 
      class="card" 
      [ngClass]="{'mb-2': marginY}"
      style="width: 100%;"
    >
      <img 
        class="card-img-top"
        *ngIf="img"
        [src]="img" 
        [alt]="title"
      >
      <div class="card-body">
        <h5 class="card-title">{{title}}</h5>
        <p class="card-text">
          <ng-content></ng-content>
        </p>
        <a 
          (click)="buttonClick.emit()" 
          *ngIf="buttonLabel"
          class="btn"
          [ngClass]="'btn-' + buttonColor"
          [style.background-color]="buttonColor"
        > {{buttonLabel}}</a>
      </div>
    </div>
  `,
})
export class CardComponent {
  @Input() marginY: boolean = false
  @Input() img: string | undefined;
  @Input() title: string | undefined;
  @Input() description: string | undefined;
  @Input() buttonLabel: string | null = null;
  @Input() buttonColor: 'primary' | 'success' | 'danger' = 'primary';
  @Input() buttonUrl: string | undefined;
  @Output() buttonClick = new EventEmitter();

}
