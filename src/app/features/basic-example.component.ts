import { Component } from '@angular/core';
import { Gender, User } from '../model/user';

@Component({
  selector: 'app-users',
  template: `
    <input
      type="text"
      placeholder="name"
      #inputName
      (keydown.enter)="addUser(inputName, inputAge)"
    >

    <input
      type="number"
      placeholder="age"
      #inputAge
      (keydown.enter)="addUser(inputName, inputAge)"
    >

    <button>Save</button>

    <li *ngFor="let user of users; let i = index">
      {{i+1}} ) -  {{user.name}} - {{user.age}}
      <i class="fa fa-trash" (click)="deleteUser(user)"></i>
    </li>
  `
})
export class BasicExampleComponent {
  users: Partial<User>[] = [];

  constructor() {
    setTimeout(() => {
      this.users = [
        {
          id: 1,
          name: 'Fabio',
          age: 30,
          gender: 'M',
          city: 'Trieste',
          birthday: 1598910648000,
          bitcoins: 1.235
        },
        {
          id: 2,
          name: 'Silvia',
          age: 24,
          bitcoins: 2.12341412412412,
          gender: 'F',
          birthday: 1593910648000,
          city: 'Roma'
        },
        {
          id: 3,
          name: 'Lorenzo',
          age: 29,
          bitcoins: 1.2323523523,
          gender: 'M',
          birthday: 1591910648000,
          city: 'Milan'
        },
      ];
    }, 1000)
  }

  deleteUser(user: Partial<User>) {
    const index = this.users.findIndex(u => u.id === user.id)
    this.users.splice(index, 1)
  }


  addUser(inputName: HTMLInputElement, inputAge: HTMLInputElement) {
    this.users.push({ name: inputName.value, age: +inputAge.value})
    inputName.value = '';
    inputAge.value = '';
  }

  doSomething(gender: Gender) {
  }

}
