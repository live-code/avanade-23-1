import { Component } from '@angular/core';
import { CardComponent } from '../../shared/components/card.component';
import { HelloComponent } from '../../shared/components/hello.component';

@Component({
  selector: 'app-uikit',
  template: `
    <h1>UIKIT: Card</h1>
    
    <div style="width: 200px">
      <app-map-quest [coords]="{ lat: '43', lng: '13'}"></app-map-quest>
      <app-map-quest [zoom]="10" [coords]="{ lat: '43', lng: '13'}"></app-map-quest>
    </div>
    <!--<button disabled (click)="...">ciao</button>-->
    <div class="container">
      <app-card
        title="Avanade 1"
        img="https://cdn.pixabay.com/photo/2012/08/27/14/19/mountains-55067__340.png"
        buttonLabel="visit website"
        buttonColor="danger"
        [marginY]="true"
        (buttonClick)="visitWebsite()"
      >
       Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci architecto asperiores, autem deserunt dignissimos doloremque eos eveniet ex harum hic in, minima minus officiis quam sequi ut vel velit? Rem.
      </app-card>
      
      <app-card 
        title="Google Profile"
        buttonLabel="Visit profile"
        buttonUrl="https://www.google.com"
        (buttonClick)="counter = counter + 1"
      >
       Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eligendi, ex fuga laudantium nam necessitatibus repellat! Accusamus beatae dolorem doloremque dolores, et facere itaque nesciunt nobis quam, quod rerum soluta.
      </app-card>
      
     
      <app-card title="simple card">
       Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eligendi, ex fuga laudantium nam necessitatibus repellat! Accusamus beatae dolorem doloremque dolores, et facere itaque nesciunt nobis quam, quod rerum soluta.
      </app-card>

      <h1>{{counter}}</h1>
     
      
      <h1>UIKIT: hello</h1>
      <app-hello size="50"></app-hello>
      <app-hello [size]="40"></app-hello>
      <app-hello name="Pluto" color="red"></app-hello>
      <app-hello name="Topolino" color="blue"></app-hello>
    </div>
  `,
})
export class UikitComponent {
  myLabel: string | null = 'visit web site'
  counter = 0;

  visitWebsite() {
    window.open('http://www.avanade.com')
  }
}

