import { Component } from '@angular/core';
import { interval, share } from 'rxjs';
import { ThemeService } from '../core/services/theme.service';

@Component({
  selector: 'app-settings',
  template: `
    <p>
      settings works!
    </p>
    
    
    <h1>Cambia tema ({{themeService.theme}})</h1>
    <button (click)="themeService.theme = 'dark'">dark</button>
    <button (click)="themeService.theme = 'light'">light</button>
  `,
})
export class SettingsComponent {

  constructor(public themeService: ThemeService) {
  }

}
