import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { JUser } from '../../model/juser';

@Component({
  selector: 'app-users-details',
  template: `
    <p>
      users-details works!
    </p>
    
    <h1>{{user?.name}}</h1>
    
    <form #f="ngForm" (submit)="save(f)">
      <input type="text" [ngModel]="user?.name" name="name">
      <input type="text" [ngModel]="user?.email" name="email">
      <input type="text" [ngModel]="user?.website" name="website">
      <button type="submit">save</button>
    </form>
  `,
  styles: [
  ]
})
export class UsersDetailsComponent {
  user: JUser | undefined;

  constructor(
    private activatedRouted: ActivatedRoute,
    private http: HttpClient
  ) {
    const id = activatedRouted.snapshot.params['userId'];
    this.http.get<JUser>(`http://localhost:3000/users/${id}`)
      .subscribe(res => this.user = res)

  }

  save(f: NgForm) {
    this.http.patch<JUser>(`http://localhost:3000/users/${this.user?.id}`, f.value)
      .subscribe(res => this.user = res)

  }
}
