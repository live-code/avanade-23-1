import { Component, ViewEncapsulation } from '@angular/core';
import { NavigationService } from '../../core/services/navigation.service';
import { JUser } from '../../model/juser';
import { SidepanelService } from './services/sidepanel.service';
import { UsersService } from './services/users.service';

@Component({
  selector: 'app-crud-example',
  template: `
    {{sidePanelService.isPanelOpened}}
    <button (click)="addNewUser()">Add user</button>
    
    <app-side-panel 
      (close)="usersService.setActiveUser(null)"
    >
      <app-users-form
        [activeUser]="usersService.activeUser"
        (save)="saveHandler($event)"
        (resetUser)="usersService.setActiveUser(null)"
      ></app-users-form>
    </app-side-panel>
  
    <app-users-list 
      [users]="usersService.users"
      [activeUser]="usersService.activeUser"
      (deleteUser)="usersService.deleteUser($event)"
      (selectUser)="setActiveUser($event)"
      (editUser)="navigationService.goto('/users/' + $event)"
    ></app-users-list>
  `,

})
export class UsersComponent {
  constructor(
    public usersService: UsersService,
    public sidePanelService: SidepanelService,
    public navigationService: NavigationService
  ) {
    usersService.getUsers()
  }

  setActiveUser(user: JUser | null) {
    this.sidePanelService.openPanel();
    this.usersService.setActiveUser(user)
  }


  saveHandler(formData: any) {
    this.usersService.save(formData)
      .then((res) => {
        // form.reset();
        this.closePanel()
      })
  }

  addNewUser() {
    this.sidePanelService.openPanel()
    this.usersService.setActiveUser(null)
  }

  closePanel() {
    window.alert('chiuso!')
    // this.sidePanelService.closePanel()
    this.usersService.setActiveUser(null)
  }
}
