import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { delay } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { JUser } from '../../../model/juser';



@Injectable({ providedIn: 'root'})
export class UsersService {
  users: JUser[] = [];
  activeUser: JUser | null = null;

  constructor(private http: HttpClient) {}

  getUsers() {
    console.log(environment.production)
    this.http.get<JUser[]>(`${environment.BASE_PATH}/users`)
      .subscribe(res => this.users = res)
  }

  deleteUser(idToRemove: number) {
    this.http.delete<void>(`${environment.BASE_PATH}/users/${idToRemove}`)
      .subscribe({
        next: () => {
          this.users = this.users.filter(u => u.id !== idToRemove)

          if (this.activeUser?.id === idToRemove) {
            this.setActiveUser(null)
          }
        },
        error: (err: HttpErrorResponse) => {
          console.log(err)
        },
      })
  }

  save(user: JUser) {
    if (this.activeUser?.id) {
      return this.editUser(user)
    } else {
      return this.addUser(user)
    }
  }

  editUser(user: JUser): Promise<JUser> {
    return new Promise((resolve, reject) => {
      this.http.patch<JUser>(`${environment.BASE_PATH}/users/${this.activeUser?.id}`, user)
        .subscribe(res => {
          const index = this.users.findIndex(u => u.id === this.activeUser?.id)
          // this.users[index] = res;
          this.users = this.users.map(u => {
            return u.id === this.activeUser?.id ? res : u
          })
          resolve(res)
          this.setActiveUser(null)
          // this.closePanel()
        })
    })
  }

  addUser(user: JUser): Promise<JUser> {
    return new Promise((resolve, reject) => {
      this.http.post<JUser>(`${environment.BASE_PATH}/users/`, user)
        .subscribe(res => {
          // this.users.push(res);
          this.users = [...this.users, res]
          // this.users = this.users.concat([res])
          //  f.reset();
          resolve(res);
          this.setActiveUser(null)
        })
    })
  }

  setActiveUser(user: JUser | null) {
    this.activeUser = user;
  }


}
