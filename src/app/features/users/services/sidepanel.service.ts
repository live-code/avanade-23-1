import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidepanelService {
  isPanelOpened = false;

  openPanel() {
    this.isPanelOpened = true;
  }

  closePanel() {
    this.isPanelOpened = false;
  }

}
