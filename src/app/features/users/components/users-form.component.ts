import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { JUser } from '../../../model/juser';

@Component({
  selector: 'app-users-form',
  template: `
    <form #f="ngForm" (submit)="save.emit(f.value)">
      <input type="text" [ngModel]="activeUser?.name" name="name" required>
      <input type="text" [ngModel]="activeUser?.website" name="website" required>
      <button type="submit" [disabled]="f.invalid">
        {{activeUser?.id ? 'EDIT' : 'ADD'}}
      </button>
      <button
        *ngIf="activeUser"
        type="button" (click)="resetUser.emit()">clear</button>
    </form>
  `,
})
export class UsersFormComponent {
  @Input() activeUser: JUser | null = null;
  @Output() save = new EventEmitter<any>();
  @Output() resetUser = new EventEmitter();
}
