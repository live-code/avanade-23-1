import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { JUser } from '../../../model/juser';

@Component({
  selector: 'app-users-list-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <li
      class="list-group-item"
      [ngClass]="{active: selected}"
    >
      
      <div 
        class=" d-flex justify-content-between align-items-center"
      >
        <div>
          <i
            class="fa fa-arrow-circle-o-down"
            (click)="toggle($event)"
          ></i>
          {{user.id}} -{{user.name}}
        </div>
  
        <div class="d-flex gap-2">
          <i class="fa fa-trash"
             (click)="deleteHandler(user.id, $event)"></i>
          <i class="fa fa-edit"
             (click)="editUser.emit(user.id)"></i>
        </div>
      </div>
      <!--body-->
      <div *ngIf="isOpened">
        {{user.website}} <br>
        <app-map-quest [coords]="user.address.geo"></app-map-quest>
      </div>
    </li>
  `,
  styles: [
  ]
})
export class UsersListItemComponent {
  @Input() user!: JUser;
  @Input() selected: boolean = false;
  @Output() deleteUser = new EventEmitter<number>()
  @Output() editUser = new EventEmitter<number>()

  isOpened = false;

  toggle(e: MouseEvent) {
    e.stopPropagation()
    this.isOpened = !this.isOpened;
  }

  deleteHandler(id: number, e: MouseEvent) {
    e.stopPropagation();
    this.deleteUser.emit(id)
  }

}
