import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { JUser } from '../../../model/juser';

@Component({
  selector: 'app-users-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <ul class="list-group">
      <app-users-list-item
        *ngFor="let user of users"
        [user]="user"
        [selected]="user.id === activeUser?.id"
        (click)="selectUser.emit(user)"
        (editUser)="editUser.emit($event)"
        (deleteUser)="deleteUser.emit($event)"
      >
        
      </app-users-list-item>
    </ul>
  `,
})
export class UsersListComponent {
  @Input() users: JUser[] = [];
  @Input() activeUser: JUser | null = null;
  @Output() deleteUser = new EventEmitter<number>()
  @Output() selectUser = new EventEmitter<JUser | null>()
  @Output() editUser = new EventEmitter<number>()


}
