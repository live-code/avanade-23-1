import { Component } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';

@Component({
  selector: 'app-form-example',
  template: `
    <h1>Template Driven Form</h1>


    <form #f="ngForm" (submit)="save(f)">

      <div *ngIf="inputName.errors">
        <label *ngIf="inputName.errors['required']">il campo name è obbligatorio</label>
        <label *ngIf="inputName.errors['minlength']">
          Missing {{inputName.errors['minlength']['requiredLength'] - inputName.errors['minlength']['actualLength']}} chars
        </label>
      </div>
      <input
        type="text"
        name="name"
        required
        minlength="5"
        placeholder="name *"
        [ngModel]="user.name"
        #inputName="ngModel"
        class="form-control"
        [ngClass]="{'is-invalid': inputName.invalid && f.dirty, 'is-valid': inputName.valid}"
      >

      <!--<error-bar [input]="inputName"></error-bar>-->
      <div class="my-bar" [style.width.%]="getPerc(inputName)"></div>

      <input
        type="text"
        [ngModel]="user.surname"
        name="surname"
        required
        minlength="3"
        placeholder="surname *"
        class="form-control"
        #inputSurname="ngModel"
        [ngClass]="{'is-invalid': inputSurname.invalid && f.dirty, 'is-valid': inputSurname.valid}"
      >

      <!--error-bar-->
      <div class="my-bar" [style.width.%]="getPerc(inputSurname)"></div>

      <input type="checkbox" [ngModel]="user.subscription" name="subscription">

      <select [ngModel]="user.gender" name="gender" required>
        <option [value]="null">Select gender</option>
        <option value="M">Male</option>
        <option value="F">Female</option>
      </select>

      <button
        type="submit"
        [disabled]="f.invalid"
        class="btn"
        [ngClass]="{'btn-success': f.valid, 'btn-primary': f.invalid}"
      >Save
      </button>
    </form>

    <pre> value: {{f.value | json}}</pre>

  `,
  styles: [`
    .my-bar {
      width: 100%;
      height: 5px;
      background-color: gray;
      border-radius: 20px;
      margin: 10px 0;
      transition: width 0.5s cubic-bezier(0.68, -0.6, 0.32, 1.6)
    }
  `]
})
export class FormExampleComponent {
  user = {  name: '', surname: '', subscription: false, gender: null}
  EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  save(form: NgForm) {
    console.log('http POST', form.value);
    form.reset()
    // form.reset({ gender: ''})
  }

  getPerc(input: NgModel) {
    if (input.errors?.['minlength']) {
      return (input.errors['minlength']['actualLength'] / input.errors['minlength']['requiredLength']) * 100
    }
    return 0
  }
}
