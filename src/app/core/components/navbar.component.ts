import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ThemeService } from '../services/theme.service';

@Component({
  selector: 'app-navbar',
 // changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div 
      class="my-navbar btn-group"
      [ngClass]="{
        'dark': themeService.theme === 'dark',
        'light': themeService.theme === 'light'
      }"
    >
      {{themeService.theme }}
      <button class="btn btn-light" routerLink="basic-example" routerLinkActive="bg-warning">basic example</button>
      <button class="btn btn-light" routerLink="form-example" routerLinkActive="bg-warning">form</button>
      <button class="btn btn-light" routerLink="users" routerLinkActive="bg-warning">users xhr</button>
      <button class="btn btn-light" routerLink="settings" routerLinkActive="bg-warning">settings</button>
      <button class="btn btn-light" routerLink="uikit" routerLinkActive="bg-warning">uikit</button>
    </div> 
    
  `,
  styles: [`
    .my-navbar {
      padding: 20px;
    }
    .dark {
      background-color: #222;
    }
    .light {
      background-color: #ccc;
    }
  `]
})
export class NavbarComponent {
  constructor(public themeService: ThemeService) {
  }

  render() {
    console.log('render navbar')
  }
}
