import { Injectable } from '@angular/core';

export type Theme = 'light' | 'dark'

@Injectable({ providedIn: 'root' })
export class ThemeService {
  private _theme: Theme = 'light';

  constructor() {
    const themeFromLocalStorage = localStorage.getItem('theme')
    if (themeFromLocalStorage) {
      this._theme = themeFromLocalStorage as Theme;
    }
  }

  set theme(theme: Theme) {
    localStorage.setItem('theme', theme)
    this._theme = theme;
  }

  get theme() {
    return this._theme
  }
}
